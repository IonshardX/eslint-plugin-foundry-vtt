# eslint-plugin-foundry-vtt

Allows using ESLint to lint Foundry VTT modules

## Installation

You'll first need to install [ESLint](http://eslint.org):

```
$ npm i eslint --save-dev
```

Next, install `eslint-plugin-foundry-vtt`:

```
$ npm install eslint-plugin-foundry-vtt --save-dev
```

**Note:** If you installed ESLint globally (using the `-g` flag) then you must also install `eslint-plugin-foundry-vtt` globally.

## Usage

Add `plugin:foundry-vtt/recommended` to the extends section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
    "extends": [
        "plugin:foundry-vtt/recommended"
    ]
}
```
